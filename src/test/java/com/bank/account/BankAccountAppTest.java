package com.bank.account;

import com.bank.account.config.AccountOperationLimits;
import com.bank.account.domain.Account;
import com.bank.account.repository.AccountRepository;
import com.bank.account.repository.TransactionRepository;
import com.bank.account.service.exception.AccountOperationError;
import com.bank.account.web.dto.BalanceResponse;
import com.bank.account.web.dto.DepositRequest;
import com.bank.account.web.dto.DepositResponse;
import com.bank.account.web.dto.ErrorResponse;
import com.bank.account.web.dto.WithdrawalRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.transaction.annotation.Transactional;

import static com.bank.account.config.AppConfig.ACCOUNT_ID;
import static org.assertj.core.api.Assertions.assertThat;

@Transactional
@AutoConfigureWebTestClient
@SpringBootTest
class BankAccountAppTest {

    private static final long ACCOUNT_INITIAL_BALANCE = 1000;

    @Autowired
    private AccountOperationLimits limits;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private WebTestClient webTestClient;

    @BeforeEach
    void beforeEach() {
        transactionRepository.deleteAll();
        setAccountBalance(ACCOUNT_INITIAL_BALANCE);
    }

    private void setAccountBalance(long balance) {
        Account account = accountRepository.getOne(ACCOUNT_ID);
        account.setBalance(balance);
        accountRepository.save(account);
    }

    @Test
    void testGetBalance() {
        long balance = getCurrentBalance();
        assertThat(balance).isNotNull();
    }

    @Test
    void testDeposit_Success() {
        long initialBalance = getCurrentBalance();

        long depositAmount = 1L;
        credit(depositAmount)
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody(DepositResponse.class)
                .value(tx -> assertThat(tx.getAmount()).isEqualTo(depositAmount));

        long finalBalance = getCurrentBalance();
        assertThat(finalBalance).isEqualTo(initialBalance + depositAmount);
    }

    @Test
    void testDeposit_TransactionLimit() {
        long initialBalance = getCurrentBalance();

        long depositAmount = limits.getDepositTransactionLimit() + 1;
        assertDepositValidation(depositAmount, AccountOperationError.DEPOSIT_TRANSACTION_LIMIT);

        long finalBalance = getCurrentBalance();
        assertThat(finalBalance).isEqualTo(initialBalance);
    }

    @Test
    void testDeposit_DailyLimit() {
        int operations = (int) (limits.getDepositDailyLimit() / limits.getDepositTransactionLimit());
        long rest = limits.getDepositDailyLimit() % limits.getDepositTransactionLimit();

        while (operations-- > 0) {
            credit(limits.getDepositTransactionLimit())
                    .expectStatus().isOk();
        }

        long balance = getCurrentBalance();
        assertDepositValidation(rest + 1, AccountOperationError.DEPOSIT_DAILY_LIMIT);

        long finalBalance = getCurrentBalance();
        assertThat(finalBalance).isEqualTo(balance);
    }

    @Test
    void testDeposit_TransactionFrequencyLimit() {
        long txCount = limits.getDepositTransactionFrequencyLimit();
        while (--txCount >= 0) {
            credit(1)
                    .expectStatus().isOk();
        }

        long balance = getCurrentBalance();
        assertDepositValidation(1, AccountOperationError.DEPOSIT_TRANSACTION_FREQUENCY_LIMIT);

        long finalBalance = getCurrentBalance();
        assertThat(finalBalance).isEqualTo(balance);
    }

    @Test
    void testWithdrawal_Success() {
        long initialBalance = getCurrentBalance();

        long withdrawalAmount = 1L;
        debit(withdrawalAmount)
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody(DepositResponse.class)
                .value(tx -> {
                    assertThat(tx.getAmount()).isEqualTo(withdrawalAmount);
                });

        long finalBalance = getCurrentBalance();
        assertThat(finalBalance).isEqualTo(initialBalance - withdrawalAmount);
    }

    @Test
    void testWithdrawal_InsufficientFunds() {
        long balance = limits.getWithdrawalTransactionLimit() - 1;
        setAccountBalance(balance);

        assertWithdrawalValidation(limits.getWithdrawalTransactionLimit(), AccountOperationError.WITHDRAWAL_INSUFFICIENT_FUNDS);

        long finalBalance = getCurrentBalance();
        assertThat(finalBalance).isEqualTo(balance);
    }

    @Test
    void testWithdrawal_TransactionLimit() {
        long initialBalance = getCurrentBalance();

        long withdrawalAmount = limits.getWithdrawalTransactionLimit() + 1;
        assertWithdrawalValidation(withdrawalAmount, AccountOperationError.WITHDRAWAL_TRANSACTION_LIMIT);

        long finalBalance = getCurrentBalance();
        assertThat(finalBalance).isEqualTo(initialBalance);
    }

    @Test
    void testWithdrawal_DailyLimit() {
        setAccountBalance(limits.getWithdrawalDailyLimit() + 1);

        int operations = (int) (limits.getWithdrawalDailyLimit() / limits.getWithdrawalTransactionLimit());
        long rest = limits.getWithdrawalDailyLimit() % limits.getWithdrawalTransactionLimit();

        while (operations-- > 0) {
            debit(limits.getWithdrawalTransactionLimit())
                    .expectStatus().isOk();
        }

        long balance = getCurrentBalance();
        assertWithdrawalValidation(rest + 1, AccountOperationError.WITHDRAWAL_DAILY_LIMIT);

        long finalBalance = getCurrentBalance();
        assertThat(finalBalance).isEqualTo(balance);
    }

    @Test
    void testWithdrawal_TransactionFrequencyLimit() {
        long txCount = limits.getWithdrawalTransactionFrequencyLimit();
        while (--txCount >= 0) {
            debit(1)
                    .expectStatus().isOk();
        }

        long balance = getCurrentBalance();
        assertWithdrawalValidation(1, AccountOperationError.WITHDRAWAL_TRANSACTION_FREQUENCY_LIMIT);

        long finalBalance = getCurrentBalance();
        assertThat(finalBalance).isEqualTo(balance);
    }

    private long getCurrentBalance() {
        return webTestClient.get()
                .uri("/account/{accountId}/balance", ACCOUNT_ID)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody(BalanceResponse.class)
                .returnResult().getResponseBody()
                .getAmount();
    }

    private void assertDepositValidation(long amount, AccountOperationError error) {
        credit(amount)
                .expectStatus().isEqualTo(HttpStatus.CONFLICT)
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody(ErrorResponse.class)
                .value(err -> assertError(err, error));
    }

    private WebTestClient.ResponseSpec credit(long amount) {
        return webTestClient.post()
                .uri("/account/{accountId}/deposit", ACCOUNT_ID)
                .bodyValue(new DepositRequest(amount))
                .exchange();
    }

    private void assertError(ErrorResponse response, AccountOperationError error) {
        assertThat(error.getErrorCode()).isEqualTo(error.getErrorCode());
        assertThat(error.getErrorMessage()).isEqualTo(error.getErrorMessage());
    }

    private void assertWithdrawalValidation(long amount, AccountOperationError error) {
        debit(amount)
                .expectStatus().isEqualTo(HttpStatus.CONFLICT)
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody(ErrorResponse.class)
                .value(err -> assertError(err, error));
    }

    private WebTestClient.ResponseSpec debit(long amount) {
        return webTestClient.post()
                .uri("/account/{accountId}/withdrawal", ACCOUNT_ID)
                .bodyValue(new WithdrawalRequest(amount))
                .exchange();
    }
}

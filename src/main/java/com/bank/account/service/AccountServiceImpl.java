package com.bank.account.service;

import com.bank.account.config.AccountOperationLimits;
import com.bank.account.domain.Account;
import com.bank.account.domain.Transaction;
import com.bank.account.domain.TransactionType;
import com.bank.account.repository.AccountRepository;
import com.bank.account.repository.TransactionRepository;
import com.bank.account.service.exception.AccountOperationError;
import com.bank.account.service.exception.AccountOperationException;
import com.bank.account.service.exception.NotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;

@Service
@Transactional
public class AccountServiceImpl implements AccountService {
    private final AccountRepository accountRepository;
    private final TransactionRepository transactionRepository;
    private final AccountOperationLimits accountOperationLimits;

    public AccountServiceImpl(
            AccountRepository accountRepository,
            TransactionRepository transactionRepository,
            AccountOperationLimits accountOperationLimits) {
        this.accountRepository = accountRepository;
        this.transactionRepository = transactionRepository;
        this.accountOperationLimits = accountOperationLimits;
    }

    @Override
    public Long getBalance(Long accountId) {
        return accountRepository.findById(accountId)
                .map(Account::getBalance)
                .orElseThrow(() -> new NotFoundException("Account[id=" + accountId + "] not found"));
    }

    @Override
    public Transaction debit(Long accountId, Long amount) {
        Account account = getAccountForUpdate(accountId);

        validateDebitSufficientFunds(account, amount);
        validateDebitTransactionLimit(amount);

        List<Transaction> transactions = getTodayTransactions(accountId, TransactionType.DEBIT);

        validateDebitDailyFrequencyLimit(transactions);
        validateDebitDailyLimit(amount, transactions);

        account.setBalance(account.getBalance() - amount);
        accountRepository.save(account);
        Transaction transaction = new Transaction(null, accountId, TransactionType.DEBIT, amount, Instant.now());

        return transactionRepository.save(transaction);
    }

    @Override
    public Transaction credit(Long accountId, Long amount) {
        Account account = getAccountForUpdate(accountId);

        validateCreditTransactionLimit(amount);

        List<Transaction> transactions = getTodayTransactions(accountId, TransactionType.CREDIT);

        validateCreditDailyLimit(amount, transactions);
        validateCreditDailyFrequencyLimit(transactions);

        account.setBalance(account.getBalance() + amount);
        accountRepository.save(account);
        Transaction transaction = new Transaction(null, accountId, TransactionType.CREDIT, amount, Instant.now());

        return transactionRepository.save(transaction);
    }

    private List<Transaction> getTodayTransactions(Long accountId, TransactionType transactionType) {
        return transactionRepository.findByAccountIdAndTypeAndTimestampBetween(
                accountId,
                transactionType,
                DateTimeUtil.todayStart(),
                DateTimeUtil.todayEnd());
    }

    private Account getAccountForUpdate(Long accountId) {
        return accountRepository.findForUpdateById(accountId)
                .orElseThrow(() -> new NotFoundException("Account[id=" + accountId + "] not found"));
    }

    private void validateDebitSufficientFunds(Account account, Long amount) {
        if (account.getBalance() - amount < 0) {
            throw new AccountOperationException(AccountOperationError.WITHDRAWAL_INSUFFICIENT_FUNDS);
        }
    }

    private void validateDebitDailyFrequencyLimit(List<Transaction> transactions) {
        if (transactions.size() == accountOperationLimits.getWithdrawalTransactionFrequencyLimit()) {
            throw new AccountOperationException(AccountOperationError.WITHDRAWAL_TRANSACTION_FREQUENCY_LIMIT);
        }
    }

    private void validateDebitDailyLimit(Long amount, List<Transaction> transactions) {
        long dailyAmount = transactions.stream().mapToLong(Transaction::getAmount).sum();
        if (dailyAmount + amount > accountOperationLimits.getWithdrawalDailyLimit()) {
            throw new AccountOperationException(AccountOperationError.WITHDRAWAL_DAILY_LIMIT);
        }
    }

    private void validateDebitTransactionLimit(Long amount) {
        if (amount > accountOperationLimits.getWithdrawalTransactionLimit()) {
            throw new AccountOperationException(AccountOperationError.WITHDRAWAL_TRANSACTION_LIMIT);
        }
    }

    private void validateCreditDailyFrequencyLimit(List<Transaction> transactions) {
        if (transactions.size() == accountOperationLimits.getDepositTransactionFrequencyLimit()) {
            throw new AccountOperationException(AccountOperationError.DEPOSIT_TRANSACTION_FREQUENCY_LIMIT);
        }
    }

    private void validateCreditDailyLimit(Long amount, List<Transaction> transactions) {
        long dailyAmount = transactions.stream().mapToLong(Transaction::getAmount).sum();
        if (dailyAmount + amount > accountOperationLimits.getDepositDailyLimit()) {
            throw new AccountOperationException(AccountOperationError.DEPOSIT_DAILY_LIMIT);
        }
    }

    private void validateCreditTransactionLimit(Long amount) {
        if (amount > accountOperationLimits.getDepositTransactionLimit()) {
            throw new AccountOperationException(AccountOperationError.DEPOSIT_TRANSACTION_LIMIT);
        }
    }
}

package com.bank.account.service.exception;

public enum AccountOperationError {

    DEPOSIT_DAILY_LIMIT(1001, "Exceeded Maximum Deposit Daily Limit"),
    DEPOSIT_TRANSACTION_LIMIT(1002, "Exceeded Maximum Deposit Per Transaction"),
    DEPOSIT_TRANSACTION_FREQUENCY_LIMIT(1003, "Exceeded Deposit Daily Frequency Limit"),
    WITHDRAWAL_DAILY_LIMIT(1004, "Exceeded Maximum Withdrawal Daily Limit"),
    WITHDRAWAL_TRANSACTION_LIMIT(1005, "Exceeded Maximum Withdrawal Per Transaction"),
    WITHDRAWAL_TRANSACTION_FREQUENCY_LIMIT(1006, "Exceeded Withdrawal Daily Frequency Limit"),
    WITHDRAWAL_INSUFFICIENT_FUNDS(1007, "Insufficient Funds");

    private final int errorCode;
    private final String errorMessage;

    AccountOperationError(int errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}

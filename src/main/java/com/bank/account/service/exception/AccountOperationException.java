package com.bank.account.service.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class AccountOperationException extends RuntimeException {
    private final AccountOperationError error;
}

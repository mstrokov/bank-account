package com.bank.account.service;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZoneOffset;

public class DateTimeUtil {

    public static final ZoneId UTC_ZONE = ZoneId.of("UTC");

    public static Instant todayStart() {
        LocalDate today = LocalDate.now(UTC_ZONE);
        return LocalDateTime.of(today, LocalTime.MIN).toInstant(ZoneOffset.UTC);
    }

    public static Instant todayEnd() {
        LocalDate today = LocalDate.now(UTC_ZONE);
        return LocalDateTime.of(today, LocalTime.MAX).toInstant(ZoneOffset.UTC);
    }

}

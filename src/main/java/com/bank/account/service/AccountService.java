package com.bank.account.service;

import com.bank.account.domain.Transaction;

public interface AccountService {

    Long getBalance(Long accountId);

    Transaction debit(Long accountId, Long amount);

    Transaction credit(Long accountId, Long amount);

}

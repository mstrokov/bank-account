package com.bank.account.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.Instant;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@ToString
public class Transaction {
    @Id
    @GeneratedValue
    private Long id;
    private Long accountId;
    private TransactionType type;
    private Long amount;
    private Instant timestamp;
}

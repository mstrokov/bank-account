package com.bank.account.domain;

public enum TransactionType {
    DEBIT,
    CREDIT
}

package com.bank.account.web.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WithdrawalResponse {
    private Long id;
    private Long amount;
    private Instant timestamp;
}

package com.bank.account.web.dto;

public interface AmountAware {
    Long getAmount();
}

package com.bank.account.web.mapper;

import com.bank.account.web.dto.BalanceResponse;

public interface BalanceMapper {

    BalanceResponse map(Long amount);

}

package com.bank.account.web.mapper;

import com.bank.account.domain.Transaction;
import com.bank.account.web.dto.DepositResponse;
import com.bank.account.web.dto.WithdrawalResponse;
import org.springframework.stereotype.Component;

@Component
public class TransactionMapperImpl implements TransactionMapper {

    @Override
    public WithdrawalResponse mapDebit(Transaction transaction) {
        return new WithdrawalResponse(transaction.getId(), transaction.getAmount(), transaction.getTimestamp());
    }

    @Override
    public DepositResponse mapCredit(Transaction transaction) {
        return new DepositResponse(transaction.getId(), transaction.getAmount(), transaction.getTimestamp());
    }
}

package com.bank.account.web.mapper;

import com.bank.account.web.dto.BalanceResponse;
import org.springframework.stereotype.Component;

@Component
public class BalanceMapperImpl implements BalanceMapper {

    @Override
    public BalanceResponse map(Long amount) {
        return new BalanceResponse(amount);
    }

}

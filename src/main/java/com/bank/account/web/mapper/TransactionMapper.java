package com.bank.account.web.mapper;

import com.bank.account.domain.Transaction;
import com.bank.account.web.dto.DepositResponse;
import com.bank.account.web.dto.WithdrawalResponse;

public interface TransactionMapper {

    WithdrawalResponse mapDebit(Transaction transaction);

    DepositResponse mapCredit(Transaction transaction);

}

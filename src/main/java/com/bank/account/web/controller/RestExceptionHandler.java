package com.bank.account.web.controller;

import com.bank.account.service.exception.AccountOperationError;
import com.bank.account.service.exception.AccountOperationException;
import com.bank.account.service.exception.NotFoundException;
import com.bank.account.web.dto.ErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

@Slf4j
@RestControllerAdvice
public class RestExceptionHandler {
    private static final ErrorResponse BAD_REQUEST = new ErrorResponse(2001, "Bad Request");
    private static final ErrorResponse NOT_FOUND = new ErrorResponse(2002, "Not found");

    @ResponseStatus(HttpStatus.CONFLICT)
    @ExceptionHandler(AccountOperationException.class)
    public ErrorResponse handleAccountOperationException(AccountOperationException e) {
        AccountOperationError error = e.getError();
        int errorCode = error.getErrorCode();
        String errorMessage = error.getErrorMessage();

        log.warn("Account operation error: {} {}", errorCode, errorMessage);

        return new ErrorResponse(errorCode, errorMessage);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({MethodArgumentTypeMismatchException.class, IllegalArgumentException.class})
    public ErrorResponse handleBadInputException(Exception e) {
        log.warn("Bad request: {}", e.getMessage(), e);
        return BAD_REQUEST.withErrorMessage(e.getMessage());
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(NotFoundException.class)
    public ErrorResponse handleNotFoundException(NotFoundException e) {
        return NOT_FOUND.withErrorMessage(e.getMessage());
    }

}

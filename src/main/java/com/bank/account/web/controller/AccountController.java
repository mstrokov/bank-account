package com.bank.account.web.controller;

import com.bank.account.domain.Transaction;
import com.bank.account.service.AccountService;
import com.bank.account.web.dto.AmountAware;
import com.bank.account.web.dto.BalanceResponse;
import com.bank.account.web.dto.DepositRequest;
import com.bank.account.web.dto.DepositResponse;
import com.bank.account.web.dto.WithdrawalRequest;
import com.bank.account.web.dto.WithdrawalResponse;
import com.bank.account.web.mapper.BalanceMapper;
import com.bank.account.web.mapper.TransactionMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class AccountController {

    private final AccountService accountService;
    private final TransactionMapper transactionMapper;
    private final BalanceMapper balanceMapper;

    public AccountController(AccountService accountService,
                             TransactionMapper transactionMapper,
                             BalanceMapper balanceMapper) {
        this.accountService = accountService;
        this.transactionMapper = transactionMapper;
        this.balanceMapper = balanceMapper;
    }

    @GetMapping("/account/{accountId}/balance")
    public BalanceResponse getBalance(@PathVariable("accountId") Long accountId) {
        Long balance = accountService.getBalance(accountId);
        return balanceMapper.map(balance);
    }

    @PostMapping("/account/{accountId}/deposit")
    public DepositResponse debit(
            @PathVariable("accountId") Long accountId,
            @RequestBody DepositRequest request) {

        validateAmount(request);
        Transaction transaction = accountService.credit(accountId, request.getAmount());

        return transactionMapper.mapCredit(transaction);
    }

    @PostMapping("/account/{accountId}/withdrawal")
    public WithdrawalResponse debit(
            @PathVariable("accountId") Long accountId,
            @RequestBody WithdrawalRequest request) {

        validateAmount(request);
        Transaction transaction = accountService.debit(accountId, request.getAmount());

        return transactionMapper.mapDebit(transaction);
    }

    private void validateAmount(AmountAware request) {
        if (request.getAmount() == null) {
            throw new IllegalArgumentException("Bad request");
        }
        if (request.getAmount() <= 0) {
            throw new IllegalArgumentException("Amount must be greater than 0.");
        }
    }
}

package com.bank.account.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;


@Getter
@Setter
@Configuration
@PropertySource("classpath:account-limits.properties")
@ConfigurationProperties(prefix = "app.account.operation")
public class AccountOperationLimits {
    private long depositTransactionLimit;
    private long depositDailyLimit;
    private long depositTransactionFrequencyLimit;
    private long withdrawalTransactionLimit;
    private long withdrawalDailyLimit;
    private long withdrawalTransactionFrequencyLimit;
}

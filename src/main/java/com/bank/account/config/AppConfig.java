package com.bank.account.config;

import com.bank.account.domain.Account;
import com.bank.account.repository.AccountRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
public class AppConfig {
    public static final long ACCOUNT_ID = 1L;
    public static final long ACCOUNT_INITIAL_BALANCE = 0;

    @Bean
    CommandLineRunner initDatabase(AccountRepository accountRepository) {
        return args -> {
            log.info("Account created: " + accountRepository.save(new Account(ACCOUNT_ID, ACCOUNT_INITIAL_BALANCE)));
        };
    }

}

package com.bank.account.repository;

import com.bank.account.domain.Transaction;
import com.bank.account.domain.TransactionType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

public interface TransactionRepository extends JpaRepository<Transaction, UUID> {

    List<Transaction> findByAccountIdAndTypeAndTimestampBetween(Long accountId,
                                                                TransactionType debit,
                                                                Instant from,
                                                                Instant to);

}

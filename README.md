## Bank Account Service

### Functional requirements

#### Operations

* **Get balance**
* **Deposit money**
  * daily amount limit $150k
  * transaction amount limit $40k
  * daily frequency limit 4 transactions
  
* **Withdrawal money**
  * daily limit $50k
  * transaction amount limit $20k
  * daily frequency limit 3 transactions
  * withdrawal amount <= account balance
    
### Non function requirements

Tech stack
* Java
* Spring Boot
* In memory db H2
    		
Single user mode (Account ID = 1)

No authentication

    
### Endpoints

**Get balance**

    GET /account/balance
    Request
        # Headers
        Content-Type: application/json    
        Accept: application/json
    
    Response
        # Headers
        Content-Type: application/json
        
        # Body
        {
            "amount": 1000
        }


**Deposit**

    POST /account/deposit
    Request
        # Headers
        Content-Type: application/json    
        Accept: application/json
        
        # Body
        {
            "amount": 1000
        }

    Response
        # Headers
        Content-Type: application/json
        
        # Body
        {
            "id": "b77234ab-53b4-47b6-acca-c07fb5150d4a",
            "amount": 1000,
            "timestamp": 1573114863
        }


**Withdrawal**

    POST /account/withdrawal
    Request
        # Headers
        Content-Type: application/json    
        Accept: application/json
        
        # Body
        {
            "amount": 1000
        }

    Response
        # Headers
        Content-Type: application/json
    
        # Body
        {
            "id": "b77234ab-53b4-47b6-acca-c07fb5150d4a",
            "amount": 1000,
            "timestamp": 1573114863
        }


### Error format

    {
        "errorCode": int
        "errorMessage": string
    }

### Errors

|Http code|App code|Message|
|---|----|---------|
|409|1001|Exceeded Maximum Deposit Daily Limit|
|409|1002|Exceeded Maximum Deposit Per Transaction|
|409|1003|Exceeded Deposit Daily Frequency Limit|
|409|1004|Exceeded Maximum Withdrawal Daily Limit|
|409|1005|Exceeded Maximum Withdrawal Per Transaction|
|409|1006|Exceeded Withdrawal Daily Frequency Limit|
|409|1007|Insufficient Funds|

### Run

    gradlew bootRun

http://localhost:8080/account/1/balance
